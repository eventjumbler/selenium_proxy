FROM ubuntu:16.04

RUN apt-get update --fix-missing
RUN apt-get install -y python3-pip python3-dev vim wget iputils-ping
RUN apt-get update --fix-missing
RUN apt-get install -y git cron rsyslog supervisor

ADD ./requirements.txt /requirements.txt
RUN pip3 install -r requirements.txt

RUN wget https://hyper-install.s3.amazonaws.com/hyper-linux-x86_64.tar.gz
RUN tar xzf hyper-linux-x86_64.tar.gz
RUN mv /hyper /usr/bin/hyper

ARG HYPERSH_ACCESS_KEY
ARG HYPERSH_SECRET
ENV HYPERSH_ACCESS_KEY ${HYPERSH_ACCESS_KEY}
ENV HYPERSH_SECRET ${HYPERSH_SECRET}
RUN hyper config --accesskey $HYPERSH_ACCESS_KEY --secretkey $HYPERSH_SECRET --default-region eu-central-1

# based on: https://github.com/aptible/docker-cron-example
#ADD ./refresh-cron /etc/cron.d/refresh-cron
#RUN chmod 0644 /etc/cron.d/refresh-cron
#RUN crontab /etc/cron.d/refresh-cron

#ADD ./start_cron.sh /start_cron.sh
#RUN chmod +x /start_cron.sh

#RUN touch /var/log/cron.log

RUN mkdir -p /var/log/supervisor
RUN mkdir -p /etc/supervisor/conf.d
ADD ./supervisor.conf /etc/supervisor.conf

ADD ./main /main
ADD ./proxy /proxy
#ADD ./manage.py /manage.py

#ADD ./run.sh /run.sh
#RUN chmod +x /run.sh

#ENV DJANGO_SETTINGS_MODULE django_core.settings
#ENV PYTHONPATH /
#RUN python3 manage.py makemigrations
#RUN python3 manage.py migrate

# CMD ./run.sh
# CMD ["/usr/local/bin/supervisord"]

# todo: this isn't being run when running container as daemon
CMD ["supervisord", "-c", "/etc/supervisor.conf"]
